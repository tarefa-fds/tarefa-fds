<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Us+Myst3oAw8vTAW9M+iOJFnWjUhUV/Ij/yhMeIcJRO6ZSTSk4bBTc7lcNhjyxRmYslosncjpHikXrgbDRJMhQ==');
define('SECURE_AUTH_KEY',  'F2E7CZWcZuqezSWuFYuaclFFA8jBFjYcc52sNDZCAkRh768Wb9XNILktDN6NoObLuOGL/DjstsUyg7wojsKUAA==');
define('LOGGED_IN_KEY',    'Ymd2zSVhLilyKthI3qAf3Qv6CwlFW5NrhnsStCyrIOeRxPFcdBALmbJaBCWHMNFedO2Vv1W6D6pj5+G8SziS0g==');
define('NONCE_KEY',        '40g2BJZW6JUodKCf/9jQkz5rFPFlsBkaiMvN2X9sm6vkRBNFfLcZ5vqFRlc6sYzebNDMRcMWJjLrUdR18L1ZfA==');
define('AUTH_SALT',        '+8q4pfX66P+eca2tVQRfdaBLj+Z7xxWdUOBQbL9FKUsO3fzdQTsk7oUi9tU9H+SOKAZVTEMrhDUYHyh2cHse2A==');
define('SECURE_AUTH_SALT', 'C4VBmh8iX4Uptyc8b7KMhH6mGyWKIWhltJMT5sKsiqPHBecXCe7YpdhTC3T7Jcyr2i5ycKqoRdVvP6EYaldI8w==');
define('LOGGED_IN_SALT',   'eY5o7MVRiJAbMQYqnRVuIsDCiIn7CYBwDYBnnIV/+loDMrhW4e6PqgteNIe5xJW3Mln8oooRi5k99q9Cp9mGXg==');
define('NONCE_SALT',       '5fAx9Gs7WaRZEUJuEFBmj0M5kr/DhYHiDVcKkoW4R2/XqOsmYAtAfGo/Z7DAi06dHuPwLEPOEfwqAwyu6fjrLQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
